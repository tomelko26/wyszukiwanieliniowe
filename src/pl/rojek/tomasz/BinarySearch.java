package pl.rojek.tomasz;

/**
 * Created by RENT on 2017-08-18.
 */
public class BinarySearch<T extends Comparable<T>> {

    public static int binSearch(int a[], int key) {
        int start = 0;
        int end = a.length - 1;

        while (start <= end) {
            int mid = (start + end) / 2;

            if (key == a[mid]) {
                return mid;
            }
            if (key < a[mid]) {
                end = mid - 1;
            } else {
                start = mid + 1;

            }

        }
        return -1;
    }

}