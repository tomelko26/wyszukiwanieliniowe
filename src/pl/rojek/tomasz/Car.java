package pl.rojek.tomasz;

import java.util.ArrayList;

/**
 * Created by RENT on 2017-08-18.
 */
public class Car implements Comparable<Car> {
    String marka;
    String model;

    ArrayList<Car> cars = new ArrayList<Car>();

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Car(String marka, String model) {
        this.marka = marka;
        this.model = model;
    }

    @Override
    public String toString(){
        return marka + " " + model;
    }

    @Override
    public int compareTo(Car o) {
        int result =  -1;
            if (getMarka() ==o.getMarka() && getModel() == o.getModel()){
                result = 0;
            }
            return result;
        }


}

